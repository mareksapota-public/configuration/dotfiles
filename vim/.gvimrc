" gvimrc
" This is free and unencumbered software released into the public domain.
"
" Anyone is free to copy, modify, publish, use, compile, sell, or
" distribute this software, either in source code form or as a compiled
" binary, for any purpose, commercial or non-commercial, and by any
" means.

set guifont=Hack\ Regular\ 11
" remove menu bar
set guioptions-=m
" remove toolbar
set guioptions-=T
" use vim style popup dialogs
set guioptions+=c

colorscheme molokai
