# Dotfiles

These are configuration files for many programs that I use.  As a whole they are
probably only useful to me but since all these files are in the public domain
feel free to salvage whatever you want.

## Zsh

Local customizaitons can be placed in the `~/.zshlocalrc` file.
