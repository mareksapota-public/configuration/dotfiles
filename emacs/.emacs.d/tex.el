;tex.el
;This is free and unencumbered software released into the public domain.

;Anyone is free to copy, modify, publish, use, compile, sell, or
;distribute this software, either in source code form or as a compiled
;binary, for any purpose, commercial or non-commercial, and by any
;means.

;; Mode for cross references
(when (require 'reftex nil t)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex))
;; AUCTeX mode
(when (load "auctex.el" t t t)
  ;; Recomended commands
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  ;; 4 spaces for indent
  (setq LaTeX-indent-level 4)
  ;; auto-fill mode
  (add-hook 'LaTeX-mode-hook 'auto-fill-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode))
