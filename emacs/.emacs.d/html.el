;html.el
;This is free and unencumbered software released into the public domain.

;Anyone is free to copy, modify, publish, use, compile, sell, or
;distribute this software, either in source code form or as a compiled
;binary, for any purpose, commercial or non-commercial, and by any
;means.

;; Loads html-mode-map definition.
(require 'sgml-mode)
(define-key html-mode-map (kbd "TAB") 'tab-to-tab-stop)

(add-hook 'html-mode-hook (lambda () (set-tab-stop-width 2)))
