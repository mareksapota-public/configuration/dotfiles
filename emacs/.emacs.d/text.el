;text.el
;This is free and unencumbered software released into the public domain.

;Anyone is free to copy, modify, publish, use, compile, sell, or
;distribute this software, either in source code form or as a compiled
;binary, for any purpose, commercial or non-commercial, and by any
;means.

(define-key text-mode-map (kbd "TAB") 'tab-to-tab-stop)
(add-hook 'text-mode-hook '(lambda()
  (setq truncate-lines nil) ;; wrap long lines
  (set-tab-stop-width 4)))

(define-derived-mode git-commit-mode text-mode
  (setq mode-name  "git commit")
  (setq-default fill-column 72))

(add-to-list 'auto-mode-alist '("/COMMIT_EDITMSG\\'" . git-commit-mode))
