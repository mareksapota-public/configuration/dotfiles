;lisp.el
;This is free and unencumbered software released into the public domain.

;Anyone is free to copy, modify, publish, use, compile, sell, or
;distribute this software, either in source code form or as a compiled
;binary, for any purpose, commercial or non-commercial, and by any
;means.

(define-key emacs-lisp-mode-map (kbd "TAB") 'tab-to-tab-stop)
(define-key lisp-mode-map (kbd "TAB") 'tab-to-tab-stop)

(defun lisp-indent-setup () (set-tab-stop-width 2))
(add-hook 'scheme-mode-hook 'lisp-indent-setup)
(add-hook 'lisp-mode-hook 'lisp-indent-setup)
(add-hook 'emacs-lisp-mode-hook 'lisp-indent-setup)
