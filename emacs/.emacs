;emacs
;This is free and unencumbered software released into the public domain.

;Anyone is free to copy, modify, publish, use, compile, sell, or
;distribute this software, either in source code form or as a compiled
;binary, for any purpose, commercial or non-commercial, and by any
;means.

(load-file "~/.emacs.d/common.el")
(load-file "~/.emacs.d/html.el")
(load-file "~/.emacs.d/javascript.el")
(load-file "~/.emacs.d/lisp.el")
(load-file "~/.emacs.d/tex.el")
(load-file "~/.emacs.d/text.el")
(load-file "~/.emacs.d/sh.el")
(load-file "~/.emacs.d/skeletons.el")
