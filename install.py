#!/usr/bin/env python3
# install.py
# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

import argparse
import os
import pathlib
import subprocess
import sys

def install_package(
    package: str,
    target: str,
    pretend: bool,
    delete: bool,
) -> bool:
    cwd = str(pathlib.Path(__file__).parent.absolute())
    cmd = filter(
        lambda x: x != '',
        [
            'stow',
            '--verbose',
            '--simulate' if pretend else '',
            f'--dir={cwd}',
            f'--target={target}',
            '--delete' if delete else '',
            package,
        ],
    )
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError:
        return False
    return True

parser = argparse.ArgumentParser(description='Install dotfiles')
parser.add_argument(
    '--pretend',
    action='store_true',
    help='Print out actions that would have been taken',
)
parser.add_argument(
    '--delete',
    action='store_true',
    help='Uninstall installed symlinks',
)
parser.add_argument(
    '--target',
    action='store',
    default=os.getenv('HOME'),
    help='Install location, defaults to the home dir',
)
parser.add_argument(
    'packages',
    metavar='PACKAGE',
    nargs='*',
    help='Packages to install',
    default=[
        'alacritty',
        'emacs',
        'git',
        'guile',
        'hg',
        'podman',
        'ruby',
        'screen',
        'vim',
        'xdg-open',
        'zsh',
    ],
)

args = parser.parse_args()

errors = []
for package in args.packages:
    success = install_package(package, args.target, args.pretend, args.delete)
    if not success:
        errors.append(package)

if len(errors) > 0:
    print('Failed projects:', ', '.join(errors))
    sys.exit(1)
