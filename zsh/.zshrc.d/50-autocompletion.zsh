# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# Auto completion.
autoload -U compinit
compinit
zmodload zsh/complist
# Only display menu, don't cycle completions with next TAB.
unsetopt auto_menu
# Always show completions even if they share a matching prefix.
unsetopt listambiguous
# Always show completions even if there is an exact match.
unsetopt rec_exact
