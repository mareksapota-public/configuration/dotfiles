# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

autoload -U add-zsh-hook

# Set GNU Screen/urxvt title to #{LAST_CMD}.
function _dotfiles_set_title() {
    # -P expands variables
    # -n doesn't print a new line
    # -r doesn't expand any characters
    local TITLE_CWD="$(print -Pn "%~")"
    local TITLE_CMD="${1}"
    local TITLE="${TITLE_CWD}:${TITLE_CMD}"
    case "${TERM}" in
    screen*)
        print -Pn "\ek"
        print -rn "${TITLE}"
        print -Pn "\e\\"
        ;;
    xterm*|rxvt*|alacritty)
        print -Pn "\e]2;"
        print -rn "${TITLE}"
        print -Pn "\a"
    esac
}

add-zsh-hook preexec _dotfiles_set_title
# Redraw the title so the pwd is right after commands like cd.  The command will
# be empty and won't be properly set.
add-zsh-hook chpwd _dotfiles_set_title

# Set title for new shells.
_dotfiles_set_title
