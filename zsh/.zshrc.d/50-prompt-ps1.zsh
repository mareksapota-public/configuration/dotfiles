# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

autoload -U add-zsh-hook
# Allows use of $fg and $fg_bold.
autoload -U colors
colors

# Set a custom shell prompt.
function _dotfiles_set_prompt() {
    # fg_bold gives a bright color.
    local BLUE="%{$fg_bold[blue]%}"
    local GREEN="%{$fg_bold[green]%}"
    local RED="%{$fg_bold[red]%}"
    local YELLOW="%{$fg_bold[yellow]%}"
    local MAGENTA="%{$fg_bold[magenta]%}"
    local NORMAL="%{$reset_color%}"
    # Main prompt color - red for root, green for normal users, yellow for test
    # shells.
    local MAIN
    if [[ -z "${ZDOTDIR}" ]]; then
        MAIN="%(!.${RED}.${GREEN})"
    else
        MAIN="${YELLOW}"
    fi

    local CURRENT_HOST='%m'
    if [[ -n "${CONTAINER_ID}" ]]; then
        CURRENT_HOST="${CONTAINER_ID}"
        MAIN="${MAGENTA}"
    fi

    local TERM_WIDTH="${COLUMNS}"
    # Minimum number of characters after a single line prompt.
    local MIN_LEFT=60
    # Leave 3 chars for " $ " or " # ".
    local WRAP_AT="$[ $max(0, ${TERM_WIDTH} - ${MIN_LEFT} - 3) ]"

    # https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
    # Mind the new line - it is a part of this prompt.
    PROMPT="${MAIN}%n@${CURRENT_HOST} ${BLUE}%~%${WRAP_AT}(l.
. )${MAIN}%(!.#.\$) ${NORMAL}"
}

add-zsh-hook precmd _dotfiles_set_prompt
