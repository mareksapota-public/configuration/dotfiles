# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# Confirm ! commands.
setopt hist_verify

# Don't remove slashes ending directory names.
unsetopt auto_remove_slash

# Don't remove spaces on `|`.
export ZLE_REMOVE_SUFFIX_CHARS=''

# '#' Triggers comments in command line.
setopt interactivecomments

# Don't replace 'cd' with 'pushd'.
unsetopt autopushd

# Don't replace executing a binary 'X' with 'cd X' if directory with the name
# 'X' exists.
unsetopt auto_cd

# Use extended globbing features.
setopt extended_glob
