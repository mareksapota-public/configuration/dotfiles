# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

autoload -U add-zsh-hook

function _dotfiles_ssh_agent_reload () {
    # Exit if ssh-agent is not installed.
    command -v ssh-agent > /dev/null || return
    # Start ssh-agent.
    local PGREP_USER="${USER-$(whoami)}"
    local SSH_AGENT_PGREP_OUTPUT="$(
        pgrep -U "${PGREP_USER}" -u "${PGREP_USER}" -x ssh-agent
    )"
    # -a = array type
    local -a SSH_AGENT_PIDS
    if [[ -z "${SSH_AGENT_PGREP_OUTPUT}" ]]; then
        SSH_AGENT_PIDS=()
    else
        SSH_AGENT_PIDS=("${(f)SSH_AGENT_PGREP_OUTPUT}")
    fi
    local SSH_HOSTNAME="${HOSTNAME-$(hostname -f)}"
    # Separate file for each hostname in case HOME is on NFS.
    local SSH_AGENT_FILE="${HOME}/.ssh/${SSH_HOSTNAME}-agent.sh"
    test -d "${HOME}/.ssh/" || mkdir -p "${HOME}/.ssh/"
    local SSH_DIR_PERMISSONS
    if [[ "${OSTYPE}" == 'linux-gnu' ]] || [[ "${OSTYPE}" == 'linux-musl' ]] || [[ -z "${OSTYPE}" ]]; then
        # Linux
        SSH_DIR_PERMISSONS="$(stat -L -c '%a' "${HOME}/.ssh/")"
    else
        # Darwin
        SSH_DIR_PERMISSONS="$(stat -L -f '%A' "${HOME}/.ssh/")"
    fi
    if [[ "${SSH_DIR_PERMISSONS}" != '700' ]]; then
        chmod 0700 "${HOME}/.ssh"
    fi
    test -f "${SSH_AGENT_FILE}" && \
        source "${SSH_AGENT_FILE}" > /dev/null || \
        touch "${SSH_AGENT_FILE}"
    local NUM_PIDS="${#SSH_AGENT_PIDS[@]}"
    # Terminate other SSH agents for the user/hostname.
    for PID in ${SSH_AGENT_PIDS[@]}; do
        if [[ "${SSH_AGENT_PID}" != "${PID}" ]]; then
            NUM_PIDS=$[${NUM_PIDS} - 1]
            kill "${PID}"
        fi
    done
    # Start SSH agent if none are running.
    if [[ ${NUM_PIDS} -eq 0 ]]; then
        ssh-agent -s > "${SSH_AGENT_FILE}"
        source "${SSH_AGENT_FILE}" > /dev/null
    fi
}

add-zsh-hook preexec _dotfiles_ssh_agent_reload

# Start SSH agent for new shells.
_dotfiles_ssh_agent_reload
