# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# Fancy Git log with commit graph.
function gitglog {
    local -a REV_RANGE=()
    if [[ -n "${1}" ]]; then
        if [[ "${1}" =~ '^[0-9]{1,6}$' ]]; then
            # Integer up to 6 characters in length, assume it's not a SHA hash.
            # Short hashes are 7 characters in length.
            REV_RANGE=("HEAD~${1}..HEAD")
        elif [[ ! "${1}" =~ '\.\.' ]]; then
            # No .. in range, assume only one end has been given.
            # ~ goes back one revision so ${1} commit is included in the output.
            REV_RANGE=("${1}~..HEAD")
        else
            # Take the range as is.
            REV_RANGE=("${1}")
        fi
    fi
    git log --graph --all --abbrev-commit --pretty=format:'%C(yellow)%h%C(reset) - %C(green)%d%C(reset) %s %C(green)(%cr)' "${REV_RANGE[@]}" --
}
