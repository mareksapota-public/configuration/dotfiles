# zshrc
# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

ZSHRC_PATH="${(%):-%N}"

() {
    local ZSHRC_D_DIR="${ZSHRC_PATH}.d/"
    local CONFIG_FILE

    if [[ -d "${ZSHRC_D_DIR}" ]]; then
        for CONFIG_FILE in ${ZSHRC_D_DIR}/*.zsh(.); do
            source "${CONFIG_FILE}"
        done
    fi
}

unset ZSHRC_PATH

# Load local configuration.
[[ -s "${HOME}/.zshlocalrc" ]] && source "${HOME}/.zshlocalrc"
