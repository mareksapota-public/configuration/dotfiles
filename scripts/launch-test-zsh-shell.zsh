#!/usr/bin/env zsh
# This is free and unencumbered software released into the public domain.

# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.

# Launch a zsh shell with configuration from this repo instead of the system
# ~/.zshrc
PWD="${0:A:h}"

env ZDOTDIR="${PWD}/../zsh" zsh
